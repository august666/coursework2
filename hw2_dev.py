"""M3C 2018 Homework 2"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from m1 import nmodel as nm
from sklearn.neural_network import MLPClassifier
import time
#assumes that hw2_dev.f90 has been compiled with: f2py -c hw2_dev.f90 -m m1
# May also use scipy, scikit-learn, and time modules as needed

def read_data(tsize=60000):
    """Read in image and label data from data.csv.
    The full image data is stored in a 784 x 70000 matrix, X
    and the corresponding labels are stored in a 70000 element array, y.
    The final 70000-tsize images and labels are stored in X_test and y_test, respectively.
    X,y,X_test, and y_test are all returned by the function.
    You are not required to use this function.
    """
    print("Reading data...") #may take 1-2 minutes
    Data=np.loadtxt('data.csv',delimiter=',')
    Data =Data.T
    X,y = Data[:-1,:]/255.,Data[-1,:].astype(int)%2 #rescale the image, convert the labels to 0s and 1s (For even and odd integers)
    Data = None

    # Extract testing data
    X_test = X[:,tsize:]
    y_test = y[tsize:]
    print("processed dataset")
    return X,y,X_test,y_test
#----------------------------

def snm_test(X,y,X_test,y_test,omethod,input=(None)):
    """
    1. Fortran+python is generally more effecient than Python. As a fundamental language, Fortran
    behaves like C in many parts like the effenciency of for loop. This makes Fortran a powerful
    and flexible language, especially for high-speed computing. In Python, for loop can dramatically slow
    down the program and vectorization is not alway unavabilable. The interaction between Python and
    Fortran solves this problem and increases the overall effeciency of program

    2. In terms of disadvantage, Fortran is clearly harder to use and understanding than Python. On
    Windows, installing Fortran and interact it with Python can also be annoying. In contrast, python
    is eailer to use and can even be accessed online by jupyter
    """

    n = X.shape[0]
    fvec = np.random.randn(n+1) #initial fitting parameters
    d=X.shape[1]
    m=0
    nm.nm_x=X
    nm.nm_y=y
    if omethod==1:
        optimal_parameter=minimize(fun=nm.snmodel,jac=True,args=(d,n),x0=fvec,method="L-BFGS-B").x
    if omethod==2:
        alpha=0.1
        optimal_parameter=nm.sgd(fvec,n,m,d,alpha)




    #Add code to train SNM and evaluate testing test_error

    fvec_f = optimal_parameter #Modify to store final fitting parameters after training
    test_z=np.matmul(fvec_f[0:n],X_test)+fvec_f[n]
    test_a=1/(1+np.exp(-test_z))
    test_error = np.sum(np.abs(np.round(test_a)-y_test))/len(y_test)#Modify to store testing error; see neural network notes for further details on definition of testing error
    output =  np.matmul(fvec_f[0:n],X)+fvec_f[n]#output tuple, modify as needed
    return fvec_f,test_error,output
#--------------------------------------------

def nnm_test(X,y,X_test,y_test,m,omethod,input=(None)):
    """Train neural network model with input images and labels (i.e. use data in X and y), then compute and return testing error (in test_error)
    using X_test, y_test. The fitting parameters obtained via training should be returned in the 1-d array, fvec_f
    X: training image data, should be 784 x d with 1<=d<=60000
    y: training image labels, should contain d elements
    X_test,y_test: should be set as in read_data above
    m: number of neurons in inner layer
    omethod=1: use l-bfgs-b optimizer
    omethod=2: use stochastic gradient descent
    input: tuple, set if and as needed
    """
    n = X.shape[0]
    fvec = np.random.randn(m*(n+2)+1) #initial fitting parameters
    d=X.shape[1]
    d_test=X_test.shape[1]
    nm.nm_x=X
    nm.nm_y=y
    if omethod==1:
        optimal_parameter=minimize(fun=nm.nnmodel,jac=True,x0=fvec,args=(n,m,d),method="L-BFGS-B").x
    if omethod==2:
        alpha=0.1
        optimal_parameter=nm.sgd(fvec,n,m,d,alpha)
    #Add code to train NNM and evaluate testing error, test_error

    w_inner=np.zeros((m,n))
    b_inner=np.zeros(m)
    w_outer=np.zeros(m)
    b_outer=0

    for i in range(n):
        j=i*m
        w_inner[:,i]=optimal_parameter[j:j+m]

    b_inner = optimal_parameter[n*m:n*m+m]
    w_outer = optimal_parameter[n*m+m:n*m+2*m]
    b_outer  = optimal_parameter[n*m+2*m]




    fvec_f = optimal_parameter #Modify to store final fitting parameters after training

    z_inner=np.matmul(w_inner,X_test)
    for i1 in range(m):
        z_inner[i1,:]=z_inner[i1,:]+b_inner[i1]
    a_inner=1/(1+np.exp(-z_inner))
    z_outer=np.matmul(w_outer,a_inner)+b_outer
    test_a=1/(1+np.exp(-z_outer))
    test_error = np.sum(np.abs(np.round(test_a)-y_test))/len(y_test)#Modify to store testing error; see neural network notes for further details on definition of testing error
    output =  np.matmul(fvec_f[0:n],X)+fvec_f[n]#output tuple, modify as needed
    return fvec_f,test_error,output
#--------------------------------------------

def nm_analyze(X,y,X_test,y_test):
    """ In generally, the error rate decreases when the number of internal nodes and
    the size of training data increase. error rates stablize at around 0.08 when
    training set is large enough.
    However, since the whole process is stochastic, the model with more internal
    nodes might exhibits higher error rates than smaller models ocassionally.
    The difference between two methods we use, SGD and BFGS is also ambiguous
    in the trials I set up. SGD seems slightly better than BFGS up to 10000 points.
    But SGD in our case takes longer time than L-BFGS-B to find the minimum point.
    Some advanced stochastic gradient techniques like AdaGrad can be considered to
    improve the effeciency.
    Finally, the run time, alghough not specially measured, is more sensitive
    to the size of internal layers than size of traininhg set. This result is not surprising
    since the time complexity of internal layer size is clearly bigger than O(N). A model
    with more than 5 internal neurons is computationally costly to evaluate.
    """

    error_rate1=np.zeros((4,5))
    error_rate2=np.zeros((4,5))
    for M in range(4):
        for i in range(1,6,1):
            if (M==0):
                fvec_f_1,test_error_1,output_1=snm_test(X[:,:i*2000],y[:i*2000],X_test,y_test,omethod=1)
                error_rate1[M,i-1]=test_error_1
                fvec_f_2,test_error_2,output_2=snm_test(X[:,:i*2000],y[:i*2000],X_test,y_test,omethod=2)
                error_rate2[M,i-1]=test_error_2
            else:
                fvec_f_1,test_error_1,output_1=nnm_test(X[:,:i*2000],y[:i*2000],X_test,y_test,omethod=1,m=M)
                error_rate1[M,i-1]=test_error_1
                fvec_f_2,test_error_2,output_2=nnm_test(X[:,:i*2000],y[:i*2000],X_test,y_test,omethod=2,m=M)
                error_rate2[M,i-1]=test_error_2
    return error_rate1,error_rate2
#--------------------------------------------

def display_image(X):
    """Displays image corresponding to input array of image data"""
    n2 = X.size
    n = np.sqrt(n2).astype(int) #Input array X is assumed to correspond to an n x n image matrix, M
    M = X.reshape(n,n)
    plt.figure()
    plt.imshow(M)
    return None
#--------------------------------------------
#--------------------------------------------


if __name__ == '__main__':
    #The code here should call analyze and generate the
    #figures that you are submitting with your code
    output=nm.analyze()
    #output = nm_analyze()
